Select 
Cast(DateName(mm, activity_datetime) AS varchar) + '-' + Cast (Year(activity_datetime) AS varchar) AS Month,
Count(activity_datetime) AS No_Of_Ent,
Convert(varchar, Min(activity_datetime), 113) AS Start_Date,
Convert(varchar, Max(activity_datetime), 113) AS End_Date
From dbo.Qualforms_activitylog
Group By Year(activity_datetime), DateName(mm, activity_datetime)
Order By Min(activity_datetime)

