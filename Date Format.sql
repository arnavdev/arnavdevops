



http://www.sql-server-helper.com/tips/date-formats.aspx



USE msdb

SELECT	j.name AS Job_Name,j.description AS Description,s.step_id AS Step_ID,s.step_name AS Step_Name,s.command AS Command,
		c.name AS Catagory_Name, s.last_run_date AS Last_Run_Date, s.last_run_time AS Last_Run_Time,
		CONVERT(VARCHAR, s.last_run_duration / 60) AS Last_Run_Duration,
		CONVERT(VARCHAR, j.date_created, 100) AS Date_Created,
		CONVERT(VARCHAR, j.date_modified, 100) AS Date_Modified,
		(CASE WHEN (j.enabled) = 1 THEN 'ENABLED' ELSE 'DISABLED' END) AS Status
FROM	([w-mum-asdbld-02].[msdb].[dbo].[sysjobs] AS j 
		INNER JOIN [w-mum-asdbld-02].[msdb].[dbo].[sysjobsteps] AS s 
			ON j.job_id = s.job_id) 
		INNER JOIN [w-mum-asdbld-02].[msdb].[dbo].[syscategories] AS c
			ON j.category_id = c.category_id
ORDER BY j.name




-- Modified on 22nd Dec 2010

SELECT	j.name AS Job_Name,
		j.description AS Description,
		s.step_id AS Step_ID,
		s.step_name AS Step_Name,
		s.command AS Command,
		c.name AS Catagory_Name, 
		--Convert( varchar, s.last_run_date, 121) AS Last_Run_Date, 
		REPLACE(CONVERT(varchar, CONVERT(datetime, CONVERT(char(10), 
		CONVERT(datetime, CONVERT(varchar, last_run_date)), 101) + ' ' + REVERSE(STUFF(STUFF(REVERSE(REPLICATE('0', 6 - LEN(CONVERT(varchar, last_run_time))) + CONVERT(varchar, last_run_time)), 3, 0, ':'), 6, 0, ':'))), 109), ':000', '') AS Last_Run_Date_Time,
		CONVERT(datetime, CAST(s.last_run_date AS CHAR(10)), 101)AS Last_Run_Date,
		s.last_run_time AS Last_Run_Time,
		CONVERT(VARCHAR, s.last_run_duration / 60) AS Last_Run_Duration,
		CONVERT(VARCHAR, j.date_created, 100) AS Date_Created,
		CONVERT(VARCHAR, j.date_modified, 100) AS Date_Modified,
		(CASE WHEN (j.enabled) = 1 THEN 'ENABLED' ELSE 'DISABLED' END) AS Status
FROM	([w-mum-asdbld-02].[msdb].[dbo].[sysjobs] AS j 
		INNER JOIN [w-mum-asdbld-02].[msdb].[dbo].[sysjobsteps] AS s 
			ON j.job_id = s.job_id) 
		INNER JOIN [w-mum-asdbld-02].[msdb].[dbo].[syscategories] AS c
			ON j.category_id = c.category_id
ORDER BY j.name

