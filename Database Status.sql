SELECT	DATABASE_NAME = db_name(s_mf.database_id)
FROM	sys.master_files s_mf
WHERE	s_mf.state = 0 AND has_dbaccess(db_name(s_mf.database_id)) = 0
GROUP BY s_mf.database_id ORDER BY 1